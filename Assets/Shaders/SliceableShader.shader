Shader "Custom/SliceableShader"
{
    Properties
    {
        _ExternalColor ("ExternalColor", Color) = (1,1,1,1)
        _InternalColor ("InternalColor", Color) = (0,0,0,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        struct Input
        {
            float4 color : COLOR;
        };
        fixed4 _ExternalColor;
        fixed4 _InternalColor;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed3 c = (IN.color.rgb * _ExternalColor.rgb) + (_InternalColor.rgb * (fixed3(1,1,1) - IN.color.rgb));
            o.Albedo = c;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
