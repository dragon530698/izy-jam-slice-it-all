using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sliceable : MonoBehaviour
{
    Rigidbody rb;
    MeshFilter meshFilter;
    Plane planeXY;
    Vector3 planeNormal;
    Sliced negativeSlice, positiveSlice;
    Collider[] colliders;
    List<Vector3> planeVertices = new List<Vector3>();


    ////////////////////
    // Unity Events
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        if (rb)
        {
            // Puts rb to sleep right at awake, this way it will only move when hit by the player.
            rb.Sleep();
        }
        meshFilter = GetComponent<MeshFilter>();
        colliders = GetComponents<Collider>();
    }

    // Slice the object on trigger enter.
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("KnifeBlade"))
        {
            KnifeController knife = other.GetComponentInParent<KnifeController>();

            // Tell the knife we are being slashed.
            if (knife != null)
            {
                knife.Slash(transform.position);
            }

            // Deactivate any colliders beforehand.
            if (colliders.Length > 0)
            {
                foreach (Collider collider in colliders)
                {
                    collider.enabled = false;
                }
            }
            

            // Converts (0,0,1) in worldspace to localspace, and create a plane with it as a normal.
            // We use (0,0,1) (or Vector3.forward in this case) as it is the only angle the sliceables can be cut (by design)
            planeNormal = transform.InverseTransformDirection(Vector3.forward).normalized;
            planeXY = new Plane(planeNormal, transform.InverseTransformPoint(transform.position));

            // Get mesh data.
            int[] triangles = meshFilter.mesh.triangles;
            Vector3[] vertices = meshFilter.mesh.vertices;
            Vector2[] uvs = meshFilter.mesh.uv;

            // Instantiate slices gameobjects.
            GameObject positiveGO = new GameObject(this.gameObject.name + " Positive");
            positiveSlice = positiveGO.AddComponent<Sliced>();

            GameObject negativeGO = new GameObject(this.gameObject.name + " Negative");
            negativeSlice = negativeGO.AddComponent<Sliced>();

            // Iterate over each triangle;
            for (int i = 0; i < triangles.Length; i += 3)
            {
                // Get each vertex data on this triangle;
                int v1Index = triangles[i];
                Vector3 v1 = vertices[v1Index];
                Vector2 uv1 = uvs[v1Index];
                bool v1Side = planeXY.GetSide(v1);
                
                int v2Index = triangles[i + 1];
                Vector3 v2 = vertices[v2Index];
                Vector2 uv2 = uvs[v2Index];
                bool v2Side = planeXY.GetSide(v2);
                
                int v3Index = triangles[i + 2];
                Vector3 v3 = vertices[v3Index];
                Vector2 uv3 = uvs[v3Index];
                bool v3Side = planeXY.GetSide(v3);
                
                // Now we compare which side of the plane each vertex is, and...
                // If all vertex are on the same side, we add the triangle to the related slice.
                if (v1Side == v2Side && v2Side == v3Side)
                {
                    AddTriangle(v1Side, v1, uv1, v2, uv2, v3, uv3);
                }
                // If not, then there are vertex on both sides of the plane.
                else
                {
                    Vector3 intersection1;
                    Vector3 intersection2;

                    Vector2 intersection1UV;
                    Vector2 intersection2UV;

                    // NOTE: The triangles are created in a clock-wise order related to its vertices.

                    // If v1 is on the same side as v2
                    if (v1Side == v2Side)
                    {
                        // Get intersection points from v2 to v3 and then from v3 to v1.
                        intersection1 = GetPlaneIntersection(v2, uv2, v3, uv3, out intersection1UV);
                        intersection2 = GetPlaneIntersection(v3, uv3, v1, uv1, out intersection2UV);

                        // Create mesh triangles from the vertices;
                        AddTriangle(v1Side, v1, uv1, v2, uv2, intersection1, intersection1UV);
                        AddTriangle(v1Side, v1, uv1, intersection1, intersection1UV, intersection2, intersection2UV);

                        AddTriangle(!v1Side, intersection1, intersection1UV, v3, uv3, intersection2, intersection2UV);
                    }
                    // If v1 is on the same side as v3
                    else if (v1Side == v3Side)
                    {
                        // Get intersection points from v1 to v2 and then from v2 to v3.
                        intersection1 = GetPlaneIntersection(v1, uv1, v2, uv2, out intersection1UV);
                        intersection2 = GetPlaneIntersection(v2, uv2, v3, uv3, out intersection2UV);

                        // Create mesh triangles from the vertices;
                        AddTriangle(v1Side, v1, uv1, intersection1, intersection1UV, v3, uv3);
                        AddTriangle(v1Side, intersection1, intersection1UV, intersection2, intersection2UV, v3, v3);

                        AddTriangle(!v1Side, intersection1, intersection1UV, v2, uv2, intersection2, intersection2UV);
                    }
                    // If v1 is alone on his side
                    else
                    {
                        // Get intersection points from v1 to v2 and then from v1 to v3.
                        intersection1 = GetPlaneIntersection(v1, uv1, v2, uv2, out intersection1UV);
                        intersection2 = GetPlaneIntersection(v1, uv1, v3, uv3, out intersection2UV);

                        // Create mesh triangles from the vertices;
                        AddTriangle(v1Side, v1, uv1, intersection1, intersection1UV, intersection2, intersection2UV);

                        AddTriangle(!v1Side, intersection1, intersection1UV, v2, uv2, v3, uv3);
                        AddTriangle(!v1Side, intersection1, intersection1UV, v3, uv3, intersection2, intersection2UV);
                    }

                    // We add the intersection points to the planeVertices list, which keeps track of all points in the plane.
                    planeVertices.Add(intersection1);
                    planeVertices.Add(intersection2);
                }
            }

            // When we're done, we close each slice by creating triangles using the stored plane points;
            CreatePlaneFaces();

            // We then finish setup on each slice and destroy our gameobject (the intact, non-sliced object).
            Material[] originalMaterials = GetComponent<MeshRenderer>().materials;
            positiveSlice.FinishSetup(originalMaterials, transform, true);
            negativeSlice.FinishSetup(originalMaterials, transform, false);

            Destroy(gameObject);
        }
    }


    ////////////////////
    // Private Methods
    void CreatePlaneFaces()
    {
        Vector3 centerVertex = GetCenterVertex();

        // Iterates over every 2 points on the plane;
        // TODO: Add those vertices to a separate submesh, as to use a custom, dinamically-created material for the inside.
        for (int i = 0; i < planeVertices.Count; i += 2)
        {
            // Gets the said points;
            Vector3 firstVertex;
            Vector3 secondVertex;

            firstVertex = planeVertices[i];
            secondVertex = planeVertices[i + 1];   

            // Gets dot product between the to-be-added triangle normal and the plane normal
            float direction = Vector3.Dot(Vector3.Cross(secondVertex - centerVertex, firstVertex - centerVertex), planeNormal);

            // If our normal is parallel to the plane's normal, then the triangle is (originally) facing to the center.
            // If that's the case, we then add it to the positive slice, and the inverse of it to the negative slice.
            if (direction > 0)
            {
                positiveSlice.AddTriangle(centerVertex, Vector2.zero, firstVertex, Vector2.zero, secondVertex, Vector2.zero);
                negativeSlice.AddTriangle(centerVertex, Vector2.zero, secondVertex, Vector2.zero, firstVertex, Vector2.zero);
            }
            // If that's not the case, we add it to the negative slice, and the inverse of it to the positive slice.
            else
            {
                positiveSlice.AddTriangle(centerVertex, Vector2.zero, secondVertex, Vector2.zero, firstVertex, Vector2.zero);
                negativeSlice.AddTriangle(centerVertex, Vector2.zero, firstVertex, Vector2.zero, secondVertex, Vector2.zero);
            }
        }
    }

    // Returns the center vertex in the intersection plane
    Vector3 GetCenterVertex()
    {
        if (planeVertices.Count > 0)
        {
            Vector3 firstVertex = planeVertices[0];
            Vector3 furthestVertex = Vector3.zero;
            float distance = 0;

            // Iterates over each vertex, and store the distance to the furthest vertex from it
            foreach (Vector3 vertex in planeVertices)
            {
                float currentDistance = 0;
                currentDistance = Vector3.Distance(firstVertex, vertex);

                if (currentDistance > distance)
                {
                    distance = currentDistance;
                    furthestVertex = vertex;
                }
            }
            
            // We then return the middle point between our first vertex and the furthest vertex from it.
            return Vector3.Lerp(firstVertex, furthestVertex, 0.5f);
        }
        // If there are no vertices on the plane, we return zero.
        else
        {
            return Vector3.zero;
        }
    }

    // Interpolates UV between the two vertices, while also returning the plane intersection point between the two.
    Vector3 GetPlaneIntersection(Vector3 v1, Vector2 uv1, Vector3 v2, Vector2 uv2, out Vector2 intersectionUV)
    {
        float distance = GetDistanceToPlane(v1, v2, out Vector3 intersectionPoint);
        intersectionUV = Vector2.Lerp(uv1, uv2, distance);
        return intersectionPoint;
    }

    void AddTriangle(bool side, Vector3 v1, Vector2 uv1, Vector3 v2, Vector2 uv2, Vector3 v3, Vector2 uv3)
    {
        if (side)
        {
            positiveSlice.AddTriangle(v1, uv1, v2, uv2, v3, uv3);
        }
        else
        {
            negativeSlice.AddTriangle(v1, uv1, v2, uv2, v3, uv3);
        }
    }

    // Creates a ray using the v1-v2 line, and then raycasts using that line.
    // Returns the distance and the position of the intersection point.
    float GetDistanceToPlane(Vector3 v1, Vector3 v2, out Vector3 intersectionPoint)
    {
        Ray ray = new Ray(v1, v2 - v1);
        planeXY.Raycast(ray, out float distance);
        intersectionPoint = ray.GetPoint(distance);
        return distance;
    }
}
