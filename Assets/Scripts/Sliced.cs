using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sliced : MonoBehaviour
{
    public List<Color> colors = new List<Color>();
    List<int> triangles = new List<int>();
    List<Vector3> vertices = new List<Vector3>();
    List<Vector3> normals = new List<Vector3>();
    List<Vector2> uvs = new List<Vector2>();


    ////////////////////
    // Unity Events

    // If we are not being seen, then we self-destruct.
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }


    ////////////////////
    // Exposed Methods

    // Adds each vertex data to the mesh.
    public void AddTriangle(Vector3 v1, Vector2 uv1, Vector3 v2, Vector2 uv2, Vector3 v3, Vector2 uv3)
    {
        AddVertexData(v1, uv1);
        AddVertexData(v2, uv2);
        AddVertexData(v3, uv3);
    }

    public void FinishSetup(Material[] materials, Transform originalTransform, bool forwardKBDirection)
    {
        // Add the required components;
        MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
        MeshRenderer renderer = gameObject.AddComponent<MeshRenderer>();

        // Sets transform to be the same as the original object's transform;
        transform.localScale = originalTransform.localScale;
        transform.rotation = originalTransform.rotation;
        transform.position = originalTransform.position;

        // Initialize and setup mesh.
        Mesh newMesh = new Mesh();
        newMesh.vertices = vertices.ToArray();
        newMesh.triangles = triangles.ToArray();
        newMesh.normals = normals.ToArray();
        newMesh.uv = uvs.ToArray();
        newMesh.colors = colors.ToArray();
        newMesh.Optimize();
        newMesh.RecalculateNormals();
        // TODO: Smooth normals.
        

        // Assign mesh and materials to the filter and renderer;
        meshFilter.mesh = newMesh;
        renderer.materials = materials;

        // Create a new collider using the newly-created mesh;
        MeshCollider collider = gameObject.AddComponent<MeshCollider>();
        collider.sharedMesh = newMesh;
        collider.convex = true;

        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = true;
        
        gameObject.tag = "Sliced";
        gameObject.layer = LayerMask.NameToLayer("Sliced");

        // Adds force to the slice in the specified direction (forward or back, defined by the passed boolean).
        if (forwardKBDirection)
        {
            rb.AddForce(Vector3.forward * 5, ForceMode.VelocityChange);
        }
        else
        {
            rb.AddForce(Vector3.back * 5, ForceMode.VelocityChange);
        }
    }

    // Changes vertex color to black;
    // Used in the shader, where if the vertex color is black, then the area will have the _InteriorColor variable.
    // See Assets/Shaders/SliceableShader.shader
    // Currently not being used.
    public void SetVertexColor(Vector3 vertex)
    {
        if (colors.Count == 0)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                colors.Add(Color.white);
            }
        }
        else
        {
            int index = vertices.IndexOf(vertex);

            colors[index] = Color.black;
        }
    }


    ////////////////////
    // Private Methods
    void AddVertexData(Vector3 vertex, Vector2 uv)
    {
        vertices.Add(vertex);
        uvs.Add(uv);
        // We know that the vertex has been added at the last position, so we just get that last position.
        triangles.Add(vertices.Count - 1);
    }
}
