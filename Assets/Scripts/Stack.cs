using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// NOTE: Non-utilized script.
// TODO: Make it run in edit mode. Maybe I'll need to make an editor class for it.
public class Stack : MonoBehaviour
{
    public int stackSize = 1;
    public GameObject stackPrefab;

    void Awake()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        float yDistance = 0;

        MeshRenderer sliceableRenderer = stackPrefab.GetComponent<MeshRenderer>();
        for (int i = 0; i < stackSize; i++)
        {
            GameObject newSliceable = Instantiate<GameObject>(stackPrefab);
            newSliceable.transform.position = transform.position;
            newSliceable.transform.parent = transform;

            Vector3 newPos = newSliceable.transform.localPosition;
            newPos.y += sliceableRenderer.bounds.size.y;
            yDistance += sliceableRenderer.bounds.size.y;
            newSliceable.transform.localPosition = newPos;

            newSliceable.GetComponent<Rigidbody>().Sleep();
        }
    }
}
