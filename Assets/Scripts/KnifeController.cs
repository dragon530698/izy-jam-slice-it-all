using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum KnifeStates {
    IDLE,
    MOVING,
    SLASHING,
    KNOCKBACK
}

public class KnifeController : MonoBehaviour
{
    ////////////////////
    // Exposed Variable
    public float lowTorque = 2;
    public float highTorque = 10;
    public float horizontalForce = 2.5f;
    public float verticalForce = 5;
    public float knockbackForce = 5;
    public float minLowTorqueAngle = 30, maxLowTorqueAngle = 100;
    public float sphereRadius = 0.8f;
    public float slashCancelTimeMS = 500;
    public int maxSlashCalls = 5;


    ////////////////////
    // Private Variables
    private KnifeStates currentState = KnifeStates.MOVING;
    private bool skipRotation = false, skipNextRotation = false, isAlive = true, hasWon = false; 
    private int slashCalls = 0;
    private float lastSlashTime = 0;
    private Vector3 spawnPosition;
    private Quaternion spawnRotation;
    private Quaternion lockRotation = Quaternion.identity;

    private Rigidbody rb;


    ////////////////////
    // Unity Events
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.Sleep();
        

        // Unity's default is 7; We need to override this to have a faster spinning blade.
        rb.maxAngularVelocity = highTorque;
        spawnPosition = transform.position;
        spawnRotation = transform.rotation;

        slashCancelTimeMS /= 1000;
    }

    void Update()
    {
        CheckInput();
    }

    void FixedUpdate()
    {
        if (isAlive)
        {
            HandleStates();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hazard"))
        {
            GameOver();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        HandleCollision(collision);
    }

    void OnCollisionStay(Collision collision)
    {
        HandleCollision(collision);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sphereRadius);
    }


    ////////////////////
    // Exposed Methods
    public void Slash(Vector3 otherPosition)
    {
        // If we're past the sliced object, do nothing.
        if (transform.position.x > otherPosition.x)
        {
            currentState = KnifeStates.MOVING;
            return;
        }

        slashCalls++;

        // Else, first check if we're not trying to skip the rotation,
        // and then kill both velocities.
        if (!skipRotation)
        {
            currentState = KnifeStates.SLASHING;
            float t = (float)slashCalls / maxSlashCalls;
            lastSlashTime = 0;

            if (slashCalls == maxSlashCalls)
            {
                rb.velocity = new Vector3(0, rb.velocity.y, 0);
                rb.angularVelocity = Vector3.zero;
                transform.rotation = lockRotation;
            }
            else
            {
                Vector3 newVelocity = rb.velocity;
                newVelocity.x /= 2;
                rb.velocity = newVelocity;

                Vector3 newAngularVelocity = rb.angularVelocity;
                newAngularVelocity.z /= 2;
                rb.angularVelocity = newAngularVelocity;
            }
        }
    }


    ////////////////////
    // Private Methods
    void HandleStates()
    {
        // Reset the recent slash calls if we are not slashing.
        if (currentState != KnifeStates.SLASHING)
        {
            slashCalls = 0;
        }

        if (currentState == KnifeStates.MOVING)
        {
            Collider[] terrainColliders = new Collider[10];
            int terrainColliderCount = Physics.OverlapSphereNonAlloc(transform.position, sphereRadius, terrainColliders, LayerMask.GetMask("Terrain"));

            // We're not close to terrain. Do rotation.
            if (terrainColliderCount == 0)
            {
                HandleLowAndHighTorque();
            }

            // rb.AddForce(Vector3.right * horizontalForce, ForceMode.VelocityChange);
            rb.velocity = new Vector3(horizontalForce, rb.velocity.y, rb.velocity.z);
        }
        else if (currentState == KnifeStates.KNOCKBACK)
        {
            rb.velocity = new Vector3(-horizontalForce, rb.velocity.y, rb.velocity.z);
            HandleLowAndHighTorque();
        }
        else if (currentState == KnifeStates.SLASHING)
        {
            // Update lock rotation as long as we havent hit the max slash calls
            if (slashCalls != maxSlashCalls)
            {
                lockRotation = transform.rotation;
            }

            // Update timer, and return to movement if we have not slashed recently.
            lastSlashTime += Time.deltaTime;

            if (lastSlashTime >= slashCancelTimeMS)
            {
                currentState = KnifeStates.MOVING;
            }
        }
        else if (currentState == KnifeStates.IDLE)
        {
            // Forcesleep rigidbody while we are idle.
            rb.Sleep();
        }
    }

    void Jump()
    {
        currentState = KnifeStates.MOVING;

        // Update the skiprotation variables on jump.
        if (!skipRotation)
        {
            skipRotation = true;
        }
        else if (skipRotation && !skipNextRotation)
        {
            skipNextRotation = true;
        }
        rb.velocity = new Vector3 (horizontalForce, verticalForce, 0);
    }

    void HandleLowAndHighTorque()
    {
        // If we're in the low torque angle zone, apply low torque.
        // Also checks if we should skip this rotation cycle (in other words, if we should skip using low torque for this frame)
        if (transform.rotation.eulerAngles.x >= minLowTorqueAngle && transform.rotation.eulerAngles.x <= maxLowTorqueAngle &&
            !skipRotation)
        {
            rb.angularVelocity = Vector3.zero;
            rb.AddRelativeTorque(Vector3.right * lowTorque, ForceMode.VelocityChange);
        }
        // Else, apply high torque.
        else
        {
            rb.AddRelativeTorque(Vector3.right * highTorque, ForceMode.VelocityChange);

            // REVIEW: There probally is be a better way of handling this.
            // If our angle is near the min low torque angle value, (maybe) reset the skip rotation and (definitely) reset the skip next rotation variables.
            if (transform.rotation.eulerAngles.x >= minLowTorqueAngle - 15 && transform.rotation.eulerAngles.x < minLowTorqueAngle && skipRotation)
            {
                if (skipNextRotation)
                {
                    skipNextRotation = false;
                }
                else
                {
                    skipRotation = false;
                }
            }
        }
    }

    void CheckInput()
    {
        // Checks if we're on mobile, and use touch for input processing if we are.
        // Else, use mouse.
        if (Application.platform == RuntimePlatform.Android ||
            Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    if (hasWon)
                    {
                        Respawn();
                        return;
                    }
                
                    if (isAlive)
                    {
                        Jump();
                    }
                    else
                    {
                        Respawn();
                    }
                }
            }    
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hasWon)
                {
                    Respawn();
                    return;
                }

                if (isAlive)
                {
                    Jump();
                }
                else
                {
                    Respawn();
                }
            }
        }
    }

    void StopMovement()
    {
        // Stops movement if we are not trying to still move.
        if (!skipRotation)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            skipRotation = false;
            currentState = KnifeStates.IDLE;
        }
    }

    void GetKnockbacked()
    {
        // REVIEW: All is fine and dandy if we're on the knockback state,
        // but maybe the slashing state filter could have unintentional consequences.

        // Knockbacks ONLY if we're in the moving or idle state.
        if (currentState != KnifeStates.KNOCKBACK && currentState != KnifeStates.SLASHING)
        {
            rb.velocity = Vector3.zero;
            rb.AddForce(Vector3.up * knockbackForce, ForceMode.VelocityChange);
            currentState = KnifeStates.KNOCKBACK;
        }
    }

    void HandleCollision(Collision collision)
    {
        // Return if we're not alive or we have won the game.
        if (!isAlive || hasWon)
        {
            return;
        }

        // Call GameOver and return if we hit a hazard.
        if (collision.gameObject.CompareTag("Hazard"))
        {
            GameOver();
            return;
        }

        ContactPoint[] contacts = new ContactPoint[collision.contactCount];
        collision.GetContacts(contacts);
        bool hasBeenKb = false;
        foreach (ContactPoint contact in contacts)
        {
            if (contact.thisCollider.CompareTag("KnifeBlade"))
            {
                if (collision.collider.CompareTag("Terrain"))
                {
                    StopMovement();
                }
                else if (collision.gameObject.CompareTag("LevelEnd"))
                {
                    ReachedEnd();
                }
                
                // Break from the loop if we have hit any object with our blade.
                break;
            }
            else
            {
                // Gets knockabed and updates the boolean variable, as to not knockback more than once per event.
                if (currentState != KnifeStates.IDLE && !hasBeenKb)
                {
                    hasBeenKb = true;
                    GetKnockbacked();
                }
            }
        }
    }

    void ReachedEnd()
    {
        StopMovement();
        hasWon = true;
    }

    void GameOver()
    {
        rb.constraints = RigidbodyConstraints.FreezePositionZ;
        isAlive = false;
    }

    void Respawn()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
