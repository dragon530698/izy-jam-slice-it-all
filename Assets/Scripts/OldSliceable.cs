using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldSliceable : MonoBehaviour
{
    public GameObject intactSliceable;
    public GameObject cutSliceable;

    Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();

        intactSliceable.SetActive(true);
        cutSliceable.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("KnifeBlade"))
        {
            rb.Sleep();
            intactSliceable.SetActive(false);
            cutSliceable.SetActive(true);


            foreach (Transform child in cutSliceable.transform)
            {
                Rigidbody childRb = child.GetComponent<Rigidbody>();

                if (childRb != null)
                {
                    Vector3 forceDir = child.transform.position - other.transform.position;
                    childRb.AddForce(forceDir * 2, ForceMode.VelocityChange);
                }
            }
        }
    }
}
